import { enviroments } from "../enviroments";

class User{
    constructor(){
        this.cleanUser();
    }

    hasRoles(roles) {
        this.roles.some(rol => {
            for(rol2 in roles){
                if (rol2 == rol) { return true;}
            }
            return false;
        });
    }

    set setRoles(roles){this.roles = roles;}

    get getRoles() {return this.roles;}

    set setToken(token) { this.token = token;}

    get getToken() { return this.token;}

    set setUsername(username) {this.username = username;}

    get getUsername() {return this.username;}

    get toJson(){
        return {
            username: this.username,
            name: this.name,
            roles: this.roles,
            token: this.token
        }
    }

    saveToLocal() {
        localStorage.setItem( 'USER', JSON.stringify(this.toJson));
    }

    cleanUser() {
        this.username = '';
        this.name = '';
        this.roles = [];
        this.token = '';
    }

    deleteFromLocal() {
        localStorage.removeItem('USER');
    }

    loadFromLocal() {
        this.cleanUser();
        let userStr = localStorage.getItem('USER');
        if (userStr){
            let json = JSON.parse(userStr);
            Object.assign(this,json);
        }
    }
}

class Auth {
    constructor(_apiUri){
        this.apiUri = _apiUri;
        this.user = new User();
        this.user.loadFromLocal();
    }
     
    login(username, password) {
        return new Promise((resolve, reject) => {
            fetch(this.apiUri+'/login', {
                method: 'POST',
                body: JSON.stringify({username: username, password: password}),
                headers: this.getHeadersLogin
            }).then( response => response.json())
            .then(response => {
                this.user.setToken = response.token_seccion;
                this.user.setUsername = username;
                this.user.setRoles = response.roles;
                this.user.saveToLocal();
                resolve(true);    
            }).catch(e => {reject(false)});
        });
    }

    get getHeadersLogin() {
        return new Headers({
            'Content-Type': 'application/json'
        });
    }

    isLogin() {
        return new Promise((resolve, reject) => {
            fetch(this.apiUri+'/islogin', {
                method: 'GET',                
                headers: this.getHeadersToken
            }).then( response => response.json())
            .then(response => {
                resolve(response.data);
            }).catch(e => {reject(false)});
        });
    }

    logout() {
        return new Promise((resolve, reject) => {
            fetch(this.apiUri+'/logout', {
                method: 'GET',                
                headers: this.getHeadersToken
            }).then( response => response.json())
            .then(response => {
                this.user.deleteFromLocal();
                resolve(response.data);
            }).catch(e => {reject(false)});
        });
    }

    get getHeadersToken() {
        return new Headers({
            'token_seccion': this.user.getToken
        });
    }

    get getUser() {
        return this.user;
    }
}

const auth = new Auth(enviroments.api.host+'/login');


export function getAuth() {
    return auth;
}