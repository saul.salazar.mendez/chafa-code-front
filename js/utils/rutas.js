export class Rutas{
    constructor() {
        this.rutas = [];
        this.labelPrincipal = '';
    }

    addRuta(ruta) {
        this.rutas.push(ruta);
    }

    set setLabelPrincipal(principal) {
        this.labelPrincipal = principal;
    }

    get getRutas() {
        return this.rutas;
    }

    get getPrincipal() {
        return this.labelPrincipal;
    }
}

export class Ruta{
    constructor(label, path) {
        this.label = label;
        this.path = path;
    }    
    get getLabel() {
        return this.label;
    }
    get getPath() {
        return this.path;
    }
}