export function equal(x,y) {
    if (typeof x != typeof y) { return false;}
    if ( typeof x == 'object') {
        for (const key in x) {
            if (!y.hasOwnProperty(key)) { return false;}
            if (!equal(x[key], y[key]) ) {
                return false;
            }
        }
    }
    if ( typeof x != 'object' && x != y) { return false;}
    return true;
}