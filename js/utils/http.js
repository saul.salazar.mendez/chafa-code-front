import { enviroments } from "../enviroments";
import { getAuth } from "./auth";
export class Http{
    static post(uri, data) {
        const token = getAuth().getUser.getToken;
        return fetch(enviroments.api.host + uri, {
            method: 'POST',
            body: JSON.stringify(data, null, '\t').replace(/\\\"/g, "'"), 
            headers:{
                'Content-Type': 'application/json',
                'token_seccion': token
            }
        })
        .then( response => response.json());
    }

    static get(uri, data) {
        return fetch(enviroments.api.host + uri, {
            method: 'GET'
        })
        .then( response => response.json());
    }

    static put(uri, data) {
        const token = getAuth().getUser.getToken;
        return fetch(enviroments.api.host + uri, {
            body: JSON.stringify(data, null, '\t').replace(/\\\"/g, "'"),
            method: 'PUT',
            headers:{
                'Content-Type': 'application/json',
                'token_seccion': token
            }
        })
        .then( response => response.json());
    }
}