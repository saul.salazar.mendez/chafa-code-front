import Vue from "vue/dist/vue";
import VueRouter from "vue-router";
import { Home } from "./components/home.component";
import { Menu } from "./components/menu.component";
import { Problemas } from "./components/problemas.component";
import { AgregarProblema } from "./components/agregar-problema.component";
import { ResolverProblema } from "./components/resolver-problemas.component";
import { Login } from "./components/login.component";
import { Pricipal } from "./components/pricipal.component";

Vue.use(VueRouter);

const routes = [
    {path: '/', component: Home},
    {   
        path: '/principal', 
        component: Pricipal,
        children: [
            {
                path: '',
                component: Menu
            },
            {name: 'login', path: 'login', component: Login},
            {path: 'retos', component: Problemas},
            { path: 'retos/create', component: AgregarProblema },
            { path: 'retos/resolve/:id', component: ResolverProblema, props: true },
            { path: 'retos/:id', component: AgregarProblema, props: true } 

        ]
    }    
]

const router = new VueRouter({
    routes // short for `routes: routes`
});
//parcel index.html --public-url ./
const vm = new Vue({
    router    
}).$mount('#app');