import { Http } from "../utils/http";
import { Problema } from "../negocio/problema";
import { CodigoFuente } from "./codigo-fuente.component";
import { EditorProblemas } from "./editor-problemas.component";
import { Breadcrumb } from "./breadcrumb.component";
import { Rutas, Ruta } from "../utils/rutas";
import { getAuth } from "../utils/auth";

export const AgregarProblema = {
    template: `
    <div class="container" v-if="!cargando">
        <breadcrumb :rutas="rutas"></breadcrumb>  
        <div class="row">
            <div class="col-md-4" >             
                <editor-problemas placeholder="" :texto="problema.historia" @change="problema.historia=$event"></editor-problemas>
            </div>
            <div class="col-md-8" style="padding: 10px;">
                <div class="form-group">
                    <label>Título del problema</label>
                    <input required type="text" class="form-control" v-model="problema.titulo">                    
                </div>
                <div class="form-group">
                    <label>Nombre de la función</label>
                    <input required type="text" class="form-control" v-model="problema.nameFunction">                    
                </div>
                <div class="form-group">
                    <label>Parametros</label>
                    <input required type="text" class="form-control" v-model="problema.params">
                </div>                
                <codigo-fuente :model="problema.test" @change="problema.test = $event"></codigo-fuente>
                <div class="row space"></div>
                <button type="button" class="btn btn-outline-primary" @click="guardar(problema);" v-if="!problema.id">
                    Guardar <i class='fas fa-save'></i>
                </button>
                <button type="button" class="btn btn-outline-primary"  @click="editar(problema);" v-if="problema.id">
                    Editar <i class='fas fa-save'></i>
                </button>
                <router-link to="/principal">
                <button type="button" class="btn btn-outline-secondary" >
                    Cancelar <i class='fas fa-ban'></i>
                </button>
                </router-link>
            </div>
        </div>
    </div>`,
    props: {
        id: {
            type: String,
            default: null
        }
    },
    data: function() {
        return {
            problema: new Problema(),
            cargando: true,
            rutas: new Rutas(),
            auth: getAuth()
        }
    },
    methods: {
        guardar: function (data) {
            data['createdBy'] = this.auth.getUser.getUsername;
            Http.post('/problema/save', data ).then(response => {
                console.log(response);
            });
        },
        editar: function (data) {            
            Http.post('/problema/update', data ).then(response => {
                console.log(response);
            });
        }
    },
    mounted: function(){
        let obj = this;
        if (this.id != null) {
            Http.get('/problema/numero/'+this.id).then(response => {
                obj.problema.setProblema = response.data;
                obj.cargando = false;
            });
        } else {
            this.cargando = false;
        }
    },
    created: function() {
        this.rutas.addRuta(new Ruta('Home', '/principal'));
        if (this.id) {
            this.rutas.addRuta(new Ruta('Retos', '/principal/retos'));
            this.rutas.setLabelPrincipal = 'Editar problema';
        }else {this.rutas.setLabelPrincipal = 'Crear problema';}
        
    },
    components: {
        'codigo-fuente': CodigoFuente,
        'editor-problemas': EditorProblemas,
        'breadcrumb': Breadcrumb
    },
}