import { Breadcrumb } from "./breadcrumb.component";
import { Rutas, Ruta } from "../utils/rutas";

export const Menu = {
    template: `
    <div class="container menu">
        <breadcrumb :rutas="rutas"></breadcrumb>
        <div class="row">
            <div class="col-md-12">            
                    
                <div class="card">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="./img/codigo.png" alt="" class="rounded-circle">
                        </div>
                        <div class="col-md-10">
                                <div class="card-body">
                                        <h5 class="card-title">Challenges</h5>
                                        <p class="card-text">Aquí encontraras todos los retos la comunidad va creando para mejorar nuestros conociminetos.</p>                                  
                                        <router-link to="/principal/retos">
                                        <button type="button" class="btn btn-outline-secondary rounded-circle card-link">
                                            <i class='fas fa-paper-plane'></i>
                                        </button>
                                        </router-link> 
                                    </div>                
                        </div>
                    </div>                    
                </div>
                                        
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="row">
                        <div class="col-md-2">
                            <img src="./img/imaginacion.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">Crear problemas</h5>
                                <h6 class="card-subtitle mb-2 text-muted"></h6>
                                <p class="card-text">Usa tu imaginación para crear problemas que permitan a los demas desarrollar sus conocimientos</p>
                                <router-link to="/principal/retos/create">
                                    <button type="button" class="btn btn-outline-secondary rounded-circle card-link">
                                        <i class='fas fa-paper-plane'></i>
                                    </button>
                                </router-link> 
                            </div>
                            </div>                
                        </div>
                </div>                    
                   
            </div>
        </div>
    </div>`,
    components: {
        'breadcrumb': Breadcrumb
    },
    data: function(){
        return {
            rutas: new Rutas()
        }
    },
    created: function() {        
        this.rutas.setLabelPrincipal = 'Home';
    }
}