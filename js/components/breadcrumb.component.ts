import { Rutas } from "../utils/rutas";

export const Breadcrumb = {
    template: `
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            
            <li class="breadcrumb-item" v-for="ruta in rutas.getRutas">
                <router-link :to="''+ruta.getPath">
                    {{ruta.getLabel}}
                </router-link>
            </li>
            
            <li class="breadcrumb-item active" aria-current="page">{{rutas.getPrincipal}}</li>
        </ol>
    </nav>
    `,
    props: {
        rutas: {
            type: Rutas,
            default: 'type something great'
        },
    }
}