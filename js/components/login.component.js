import { getAuth } from "../utils/auth";
import { alertaSimple } from "../utils/alertas";

export const Login = {
    template: `
    <div class="container">
  <h2>Login</h2>
  <div>
    <div class="form-group">
      <label for="uname">Username:</label>
      <input type="text" class="form-control" id="uname" v-model="username"  placeholder="Enter username" name="uname" required>      
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" v-model="password" placeholder="Enter password" name="pswd" required>      
    </div>    
    <button class="btn btn-primary" @click="login()">Entrar</button>
  </div>
</div> 
    `,
    data: function(){
        return {
            username: '',
            password: '',
            auth: getAuth()
        }
    },
    methods: {
        login() {
            const obj = this;
            this.auth.login(this.username, this.password).then(response=> {
                obj.$router.push('/principal');
                alertaSimple('Bienvenido');
            });
        }
    }
}