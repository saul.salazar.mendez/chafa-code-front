export const Home = {
    template: `
    <div class="container home">
        <div class="row">
            <div class="col-sm-12" style="text-align: center">
            <h1>Chafa code</h1>
            Cada día un reto más fácil<br>
            <router-link to="/principal">
                <button class="btn btn-primary" href="#" role="button">Entrar</button>
            </router-link>            
            </div>            
        </div>
    </div>
    `
}