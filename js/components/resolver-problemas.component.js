import { Http } from "../utils/http";
import { CodigoFuente } from "./codigo-fuente.component";
import { Test } from "./test.component";
import { Breadcrumb } from "./breadcrumb.component";
import { Rutas, Ruta } from "../utils/rutas";
import { getAuth } from "../utils/auth";

export const ResolverProblema = {
    props: ['id'],
    data: function(){
        return {
        problema: {},
        cargando: true,
        codigo: '',
        rutas: new Rutas(),
        auth: getAuth(),
        respuestaId: ''};
    },
    mounted: function(){
        this.rutas.addRuta(new Ruta('Home', '/principal'));
        this.rutas.addRuta(new Ruta('Retos', '/principal/retos'));        
        let obj = this;        
        Http.get('/problema/numero/'+this.id).then(response => {            
            obj.problema = response.data;
            this.rutas.setLabelPrincipal = response.data.titulo;
            obj.problema.test = eval(obj.problema.test);            
            for (const test of obj.problema.test) {
                test['pass'] = false;
                test['errors'] = '';
                test['yourResult'] = null;
            }            
            obj.codigo +=`function ${obj.problema.nameFunction}(${obj.problema.params}) {
    let out;
    return out;
}
`            
            Http.get(`/problema/respuesta/${obj.id}/user/`+this.auth.getUser.getUsername).then(respuesta => {                                
                if (respuesta.data) {
                    obj.$set(obj, 'codigo', respuesta.data.codigo);
                    obj.$set(obj, 'respuestaId', respuesta.data.id);
                }
                obj.cargando = false;
            });
        });
    },
    components: {
        'codigo-fuente': CodigoFuente,
        'tests': Test,
        'breadcrumb': Breadcrumb
    },
    methods:{

    },
    template: `<div class="container resolver">
    <breadcrumb :rutas="rutas"></breadcrumb>
    <div class="row justify-content-md-center" v-if="cargando">
        <div class="col-md-12">
            <div class="spinner-grow" role="status" >
                    <span class="sr-only">Loading...</span>
            </div>			
        </div>				
    </div>	
    <div class="row" v-if="!cargando">    
        <div class="col-md-4" v-html="problema.historia">
        </div>
        <div class="col-md-8">      
                <codigo-fuente :model="codigo" @change="codigo = $event"></codigo-fuente>                
                <tests :src="codigo" :tests="problema.test" :nameFunction="problema.nameFunction" :respuestaId="respuestaId" :problemaId="problema.id"></tests>
        </div>
    </div>
</div>
    
`
}