import { getAuth } from "../utils/auth";

export const Pricipal = {
    template: `
    <div>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="#"><img src="./img/titulo.png" style="width: 20%;"></a>
    
    <div class="collapse navbar-collapse justify-content-between" id="navbar">
    <div class="navbar-nav"></div>
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="#" @click="salir()">Logout</a>
    </div>
    </div>
    
    
    </nav>

    <div class="container-fluid" style="margin-top:80px">
        <router-view></router-view>
    </div>

    </div>
    `,
    data: function() {
        return {
            auth: getAuth()
        }
    },
    mounted: function() {
        console.log("Estoy aqui");
        const obj = this;
        this.auth.isLogin().then(response=> {
            if (!response) {
                obj.$router.push('/principal/login');
            }
        });
    },
    methods: {
        salir: function(){
            const obj = this;
            this.auth.logout().then(response=> {
                obj.$router.push('/');
            });
        }
    }
}