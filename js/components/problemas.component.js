import { Http } from "../utils/http";
import { Breadcrumb } from "./breadcrumb.component";
import { Rutas, Ruta } from "../utils/rutas";
import { getAuth } from "../utils/auth";

export const Problemas = {
    data: function() {
        return {
            cargando: true,
            retos: [],
            rutas: new Rutas(),
            auth: getAuth()
        }
    },
    mounted: function(){
        let obj = this;
        this.rutas.addRuta(new Ruta('Home','/principal')); 
        this.rutas.setLabelPrincipal = 'Retos';
        Http.get('/problema/list').then(response => {
            obj.retos = response;
            obj.cargando = false;
        });
    },
    methods: {
        getFecha(fecha) {
            let x = new Date(parseInt(fecha));
            return x.toString();
        }
    },
    components: {
        'breadcrumb': Breadcrumb
    },
    template: `
<div class="container problemas">
    <breadcrumb :rutas="rutas"></breadcrumb>
	<div class="row justify-content-md-center" v-if="cargando">
		<div class="col-md-12">
			<div class="spinner-grow" role="status" >
					<span class="sr-only">Loading...</span>
			</div>			
		</div>				
	</div>	
	<div class="row" v-if="!cargando">
			<div class="col-md-4 problema" v-for="(reto, index) in retos">
					<div class="card" style="width: 100%;">
					<div class="card-body">
						<h2 class="card-title">{{reto.titulo}}</h2>
						<h6>Creado el {{getFecha(reto.created)}}</h6>		
						<router-link :to="'/principal/retos/'+reto.id" v-if="auth.getUser.getUsername == reto.createdBy">
                            <button type="button" class="btn btn-outline-secondary rounded-circle card-link">
                                <i class='fas fa-edit'></i>
                            </button>
                        </router-link> 
                        <router-link :to="'/principal/retos/resolve/'+reto.id">
                            <button type="button" class="btn btn-outline-secondary rounded-circle card-link">
                                <i class='fas fa-paper-plane'></i>
                            </button>
                        </router-link>
					</div>
					</div>
			</div>
	</div>
</div>

    `
}