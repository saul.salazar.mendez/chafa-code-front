import { equal } from "../utils/object";
import { Http } from "../utils/http";
import { getAuth } from "../utils/auth";

export const Test = {
    props: ['src', 'tests', 'nameFunction', 'respuestaId', 'problemaId'],
    data: function () {
        return {
            errores: '',
            arreglo: [                
                {
                    input: [3],
                    expectedOutput: [9],
                    pass: false,
                    errors: '',
                    yourResult: null
                }
            ],
            idRespuesta: null,
            auth: getAuth()
        }
    },
    methods: {
        getRespuesta: function(){
            return {
                codigo: this.src,
                problema_id: this.problemaId,
                createdBy: this.auth.getUser.getUsername,
                id: this.idRespuesta
            }
        },
        evalua: function (src) {
            for (let test of this.tests) {                     
                test.yourResult = null;
                test.pass = false;
                test.errors = '';
            }
            let fun;
            try {
                eval('fun ='+src.replace('function '+this.nameFunction, 'function'));
            } catch (error) {                
                this.errores = "Esta mal la función";
                return;
            }
            this.errores = '';
            for (let test of this.tests) {                     
                try {
                    test.yourResult =  fun(test.input);                
                    test.pass = equal(test.yourResult, test.expectedOutput);
                    if (!test.pass) {
                        test.errors = 'Your result is diferent to the expect output';
                    }
                } catch (error) {
                    test.pass = false;
                    test.errors = error;
                    break;
                }
            }
            let copia = [];
            while(this.tests.length > 0) {
                copia.push(this.tests.pop());
            }
            const obj = this;
            setTimeout(() => {
                while(copia.length > 0) {
                    obj.tests.push(copia.pop());
                }
            }, 100);
            if (!obj.idRespuesta) {                
                Http.post('/problema/respuesta', obj.getRespuesta()).then(respuesta => {
                    obj.idRespuesta = respuesta.data;
                });
            } else {
                Http.post('/problema/respuestaupdate', obj.getRespuesta()).then(respuesta => {
                    obj.idRespuesta = respuesta.data;
                });
            }
        }
    },
    mounted: function() {
        this.idRespuesta = this.respuestaId;
    },
    template: `<div class="accordion" id="accordionExample">
    <div class="card"  v-for="(item, index) in tests">
      <div class="card-header" :id="'heading'+index">
        <h2 class="mb-0">
          <button class="btn btn-link" type="button" data-toggle="collapse" :data-target="'#collapse'+index" aria-expanded="true" aria-controls="collapseOne">
          Test {{index+1}} <span v-if="item.pass"> - <i class='fas fa-check-circle'></i></span>
          </button>
        </h2>
      </div>
  
      <div :id="'collapse'+index" class="collapse" :aria-labelledby="'heading'+index" data-parent="#accordionExample">
        <div class="card-body">
          <pre>{{item}}</pre>
        </div>
      </div>
    </div>
    <button class="btn btn-primary" @click="evalua(src)">Evalua</button>
  </div>
    `,
    created: function() {
    },
}