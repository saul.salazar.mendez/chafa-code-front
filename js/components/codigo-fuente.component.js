import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/addon/edit/matchbrackets';
import 'codemirror/addon/edit/closebrackets';
import 'codemirror/theme/darcula.css';
import CodeMirror from 'codemirror';

export const CodigoFuente = {
    props: ['model', 'ancho', 'alto'],
    template: `
        <div>
            <textarea id="texto">{{model}}</textarea>
        </div>
    `,
    created: function() {
    },
    mounted: function() {        
        let component = this.$el.querySelector('textarea');
        let myCodeMirror = CodeMirror.fromTextArea(component, {
            lineNumbers: true,
            mode:  "javascript",
            indentUnit: 4,
            autoCloseBrackets: true,
            matchBrackets: true,
            theme : "darcula"
        });

        let ref = this;

        myCodeMirror.on('change', function(cm) {
            ref.$emit('change', cm.getValue());
        });
        
        myCodeMirror.setSize("100%", "100%");
    }
}