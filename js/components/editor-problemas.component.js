export const EditorProblemas =  {
    template: `<div>
        <slot name="custom-toolbar"></slot>
        <slot name="content"></slot>
        </div>`,
    props: {
        placeholder: {
            type: String,
            default: 'type something great'
        },        
        texto: {
            tupe: String,
            default: ''
        }
    },
    data: function() {
        return {
            toolbar: [ 
                [{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline'],
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                ['image', 'code-block']
            ]
        }
    },
    mounted() {      
        let customToolbar = this.$slots["custom-toolbar"];
        let customToolbarHTML = customToolbar ? customToolbar[0].elm : false;
        let quillCustomToolbarId = 'custom-quilljs-toolbar'
        if(customToolbarHTML) {
            customToolbarHTML.id = quillCustomToolbarId;
        }
        let quill = new Quill(this.$el, {
            theme: 'snow',
            placeholder: this.placeholder,
            modules: {
            toolbar: this.toolbar,
            }
        });
        quill.root.innerHTML = this.texto;
        quill.on('text-change', (delta, oldDelta, source) => {
            let html = quill.root.innerHTML;
            this.$emit('change', html, delta, oldDelta, source);
        });
    }
}

