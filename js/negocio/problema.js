export class Problema {
    constructor() {
        this.id = null;
        this.historia = '';
        this.nameFunction = '';
        this.params = "";
        this.test =  `[]`;
        this.tiutlo = '';
        this.createdBy = '';
    }

    set setProblema(problema) {
        this.id = problema.id;
        this.historia = problema.historia;
        this.nameFunction = problema.nameFunction;
        this.params = problema.params;
        this.test = problema.test;
        this.titulo = problema.titulo;
        this.createdBy = problema.createdBy;
    }
}